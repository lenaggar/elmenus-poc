import {
  TOGGLE_MODE,
  FETCH_INITIAL_DATA,
  CREATE_NEW_ITEM,
  UPDATE_ITEM,
  DELETE_ITEM,
  CREATE_NEW_CATAGORY,
  UPDATE_CATAGORY,
  DELETE_CATAGORY
} from './actions';

const initialState = {
  permissions: 'user-mode',

  categories: [],

  items: []
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_MODE:
      return {
        ...state,
        permissions: action.payload
      };

    case `${FETCH_INITIAL_DATA}_FULFILLED`: {
      const data = action.payload;

      // normalizing data... well, sort of, not quite :D
      const categoriesAndItems = data.reduce(
        (accum, cat) => ({
          categories: [...accum.categories, { id: cat.id, name: cat.name }],
          items: [...accum.items, ...(cat.items ? cat.items : [])]
        }),
        {
          categories: [],
          items: []
        }
      );

      return {
        ...state,
        ...categoriesAndItems
      };
    }

    case CREATE_NEW_ITEM:
      return {
        ...state,
        items: [...state.items, { ...action.payload }]
      };

    case UPDATE_ITEM:
      return {
        ...state,
        items: state.items.map((item) => {
          if (item.id === action.payload.id) {
            return { ...action.payload };
          }
          return item;
        })
      };

    case DELETE_ITEM:
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.payload)
      };

    case CREATE_NEW_CATAGORY:
      return {
        ...state,
        categories: [...state.categories, { ...action.payload }]
      };

    case UPDATE_CATAGORY:
      return {
        ...state,
        categories: state.categories.map((cat) => {
          if (cat.id === action.payload.id) {
            return { ...action.payload };
          }
          return cat;
        })
      };

    case DELETE_CATAGORY:
      return {
        ...state,
        categories: state.categories.filter(cat => cat.id !== action.payload)
      };

    default:
      return state;
  }
};

export default rootReducer;
