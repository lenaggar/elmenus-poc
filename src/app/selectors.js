/* eslint-disable import/prefer-default-export */

export const getCategories = (state) => {
  const { categories, items } = state;

  if (!categories) {
    return [];
  }

  return categories.map(cat => ({
    ...cat,
    items: items.filter(item => item.categoryId === cat.id)
  }));
};
