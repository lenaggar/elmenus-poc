import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Container, Grid, Header, Segment, Dimmer, Loader, Image, Button
} from 'semantic-ui-react';

import { toggleMode, fetchInitialData } from './actions';
import { getCategories } from './selectors';

import ListManager from './components/list-manager';
import CreateCategory from './components/modals/create-category';

class App extends Component {
  static propTypes = {
    toggleMode: PropTypes.func.isRequired,
    fetchInitialData: PropTypes.func.isRequired,

    mode: PropTypes.string.isRequired,
    categories: PropTypes.arrayOf(PropTypes.object).isRequired,
    isAdminMode: PropTypes.bool.isRequired,
    isUserMode: PropTypes.bool.isRequired
  };

  state = { fetching: false };

  componentDidMount() {
    const { fetchInitialData } = this.props;

    this.setState({ fetching: true }, () => fetchInitialData().finally(() => setTimeout(() => {
      // delay 3000ms to simulate network request and display spinner
      this.setState({ fetching: false });
    }, 3000)));
  }

  toggleEditMode = (newMode) => {
    const { mode: currentMode, toggleMode } = this.props;

    if (newMode !== currentMode) {
      toggleMode(newMode);
    }
  };

  render() {
    const {
      state: { fetching },

      props: { isAdminMode, isUserMode, categories },

      toggleEditMode
    } = this;

    return (
      <Container>
        <Header as="h1" textAlign="center">
          elmenu&#39;s proof of concept
        </Header>

        {fetching ? (
          <Segment>
            <Dimmer active inverted>
              <Loader content="Loading" />
            </Dimmer>

            <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
          </Segment>
        ) : (
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Button
                  attached="left"
                  active={isUserMode}
                  onClick={() => toggleEditMode('user-mode')}
                >
                  User Mode
                </Button>

                <Button
                  attached="right"
                  active={isAdminMode}
                  onClick={() => toggleEditMode('admin-mode')}
                >
                  Admin Mode
                </Button>
              </Grid.Column>
            </Grid.Row>

            {categories.length > 0 ? (
              <Grid.Row>
                <Grid.Column>
                  <ListManager categories={categories} />
                </Grid.Column>
              </Grid.Row>
            ) : (
              <p>Sorry, no categories!</p>
            )}

            {isAdminMode && (
              <Grid.Row>
                <Grid.Column>
                  <CreateCategory />
                </Grid.Column>
              </Grid.Row>
            )}
          </Grid>
        )}
      </Container>
    );
  }
}

const mapState = state => ({ mode: state.permissions, categories: getCategories(state) });

const mapDispatch = { toggleMode, fetchInitialData };

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  isAdminMode: stateProps.mode === 'admin-mode',
  isUserMode: stateProps.mode === 'user-mode'
});

export default connect(
  mapState,
  mapDispatch,
  mergeProps
)(App);
