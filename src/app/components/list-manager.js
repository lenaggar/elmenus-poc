import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Accordion } from 'semantic-ui-react';

import Category from './category';

class App extends Component {
  static propTypes = {
    categories: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  state = { activeCategory: '' };

  handleCategoryClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeCategory } = this.state;
    const newIndex = activeCategory === index ? '' : index;

    this.setState({ activeCategory: newIndex });
  };

  render() {
    const {
      state: { activeCategory },
      props: { categories },
      handleCategoryClick
    } = this;

    return (
      <Accordion fluid styled>
        {categories.map(category => (
          <Category
            key={category.id}
            handleClick={handleCategoryClick}
            active={activeCategory}
            {...category}
          />
        ))}
      </Accordion>
    );
  }
}

export default App;
