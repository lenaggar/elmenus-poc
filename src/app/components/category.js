import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Accordion, Icon, Grid, Menu, Card, Label, Button
} from 'semantic-ui-react';

import { deleteCategory, deleteItem } from '../actions';

import UpdateCategory from './modals/update-category';
import CreateItem from './modals/create-item';
import UpdateItem from './modals/update-item';

class Category extends Component {
  static propTypes = {
    deleteCategory: PropTypes.func.isRequired,
    deleteItem: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    handleClick: PropTypes.func.isRequired,
    active: PropTypes.string.isRequired,
    isAdminMode: PropTypes.bool.isRequired
  };

  state = {
    activeItem: ''
  };

  handleCategoryTitleClick = (...props) => {
    this.setState({ activeItem: '' }, () => this.props.handleClick(...props));
  };

  handleItemClick = (e, { id }) => this.setState({ activeItem: id });

  handleDeleteCategory = (e, id) => {
    e.stopPropagation();
    this.props.deleteCategory(id);
  };

  render() {
    const {
      state: { activeItem },
      props: {
        id, name, items, active, isAdminMode, deleteItem
      },
      handleCategoryTitleClick,
      handleItemClick,
      handleDeleteCategory
    } = this;

    const detailedItem = items.filter(item => item.id === activeItem)[0];

    return (
      <React.Fragment>
        <Accordion.Title index={id} onClick={handleCategoryTitleClick} active={active === id}>
          <Icon name="dropdown" />
          {name}

          {isAdminMode && (
            <div style={{ float: 'right' }}>
              <UpdateCategory id={id} name={name} />

              <Label color="red" onClick={e => handleDeleteCategory(e, id)}>
                x
              </Label>
            </div>
          )}
        </Accordion.Title>

        <Accordion.Content active={active === id}>
          <Grid>
            {/* items side menu */}
            <Grid.Column width={6}>
              {items.length > 0 ? (
                <Menu pointing vertical fluid>
                  {items.map(item => (
                    <Menu.Item
                      key={item.id}
                      name={item.name}
                      id={item.id}
                      active={activeItem === item.id}
                      onClick={handleItemClick}
                    />
                  ))}
                </Menu>
              ) : (
                <p>Sorry, no items in this category</p>
              )}

              {isAdminMode && <CreateItem categoryId={id} />}
            </Grid.Column>

            {/* selected item in category */}
            {detailedItem && (
              <Grid.Column width={10}>
                <Card color="green">
                  <Label floating tag>
                    {`$${detailedItem.price}`}
                  </Label>

                  <Card.Content header={detailedItem.name} />

                  <Card.Content description={detailedItem.description} />

                  {isAdminMode && (
                    <Card.Content extra>
                      <UpdateItem {...detailedItem} />

                      <Button color="red" onClick={() => deleteItem(detailedItem.id)}>
                        <Icon name="remove" />
                        Delete
                      </Button>
                    </Card.Content>
                  )}
                </Card>
              </Grid.Column>
            )}
          </Grid>
        </Accordion.Content>
      </React.Fragment>
    );
  }
}

const mapState = state => ({ mode: state.permissions });

const mapDispatch = { deleteCategory, deleteItem };

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  isAdminMode: stateProps.mode === 'admin-mode'
});

export default connect(
  mapState,
  mapDispatch,
  mergeProps
)(Category);
