import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Modal, Icon, Form, Label
} from 'semantic-ui-react';

import { updateCategory } from '../../actions';

class UpdateCategory extends React.Component {
  static propTypes = {
    updateCategory: PropTypes.func.isRequired,

    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  };

  state = {
    modalOpen: false,
    name: ''
  };

  componentDidMount() {
    const { name } = this.props;
    this.setState({ name });
  }

  handleOpen = (e) => {
    e.stopPropagation();
    this.setState({ modalOpen: true });
  };

  handleClose = (e) => {
    e.stopPropagation();
    this.setState({ modalOpen: false });
  };

  handleFieldChange = (e) => {
    const field = e.target.dataset.title;
    this.setState({ [field]: e.target.value });
  };

  handleSubmit = (e) => {
    e.stopPropagation();
    const {
      state: { name },
      props: { id, updateCategory }
    } = this;

    const item = {
      id,
      name
    };

    this.setState({ modalOpen: false }, () => updateCategory(item));
  };

  render() {
    const {
      state: { modalOpen, name },
      handleOpen,
      handleClose,
      handleFieldChange,
      handleSubmit
    } = this;

    const modalTrigger = (
      <Label color="blue" onClick={handleOpen}>
        <Icon name="edit" />
        Edit Category
      </Label>
    );

    return (
      <Modal
        trigger={modalTrigger}
        open={modalOpen}
        onClose={handleClose}
        onClick={e => e.stopPropagation()}
      >
        <Modal.Header>Edit Category</Modal.Header>

        <Modal.Content>
          <Form>
            <Form.Field>
              <label>Item Name</label>
              <input value={name} data-title="name" onChange={handleFieldChange} />
            </Form.Field>
          </Form>
        </Modal.Content>

        <Modal.Actions>
          <Button color="red" onClick={handleClose}>
            <Icon name="remove" />
            Cancel
          </Button>

          <Button color="green" onClick={handleSubmit}>
            <Icon name="checkmark" />
            Save
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapDispatch = { updateCategory };

export default connect(
  null,
  mapDispatch
)(UpdateCategory);
