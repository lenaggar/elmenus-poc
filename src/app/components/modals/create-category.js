import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Modal, Icon, Form
} from 'semantic-ui-react';

import { createNewCategory } from '../../actions';

class CreateCategory extends React.Component {
  static propTypes = {
    createNewCategory: PropTypes.func.isRequired
  };

  state = {
    modalOpen: false,
    name: ''
  };

  handleOpen = (e) => {
    e.stopPropagation();
    this.setState({ modalOpen: true });
  };

  handleClose = (e) => {
    e.stopPropagation();
    this.setState({ modalOpen: false });
  };

  handleFieldChange = (e) => {
    const field = e.target.dataset.title;
    this.setState({ [field]: e.target.value });
  };

  handleSubmit = (e) => {
    e.stopPropagation();
    const {
      state: { name },
      props: { createNewCategory }
    } = this;

    const cat = {
      id: `${Date.now()}`,
      name
    };

    this.setState({ modalOpen: false, name: '' }, () => createNewCategory(cat));
  };

  render() {
    const {
      state: { modalOpen, name },
      handleOpen,
      handleClose,
      handleFieldChange,
      handleSubmit
    } = this;

    const modalTrigger = (
      <Button color="blue" onClick={handleOpen}>
        <Icon name="add" />
        Create New Category
      </Button>
    );

    return (
      <Modal
        trigger={modalTrigger}
        open={modalOpen}
        onClose={handleClose}
        onClick={e => e.stopPropagation()}
      >
        <Modal.Header>Create New Category</Modal.Header>

        <Modal.Content>
          <Form>
            <Form.Field>
              <label>Item Name</label>
              <input value={name} data-title="name" onChange={handleFieldChange} />
            </Form.Field>
          </Form>
        </Modal.Content>

        <Modal.Actions>
          <Button color="red" onClick={handleClose}>
            <Icon name="remove" />
            Cancel
          </Button>

          <Button color="green" onClick={handleSubmit}>
            <Icon name="checkmark" />
            Create
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapDispatch = { createNewCategory };

export default connect(
  null,
  mapDispatch
)(CreateCategory);
