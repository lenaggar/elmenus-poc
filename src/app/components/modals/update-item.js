import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Modal, Icon, Form
} from 'semantic-ui-react';

import { updateItem } from '../../actions';

class UpdateItem extends React.Component {
  static propTypes = {
    updateItem: PropTypes.func.isRequired,

    id: PropTypes.string.isRequired,
    categoryId: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired
  };

  state = {
    modalOpen: false,
    name: '',
    description: '',
    price: ''
  };

  componentDidMount() {
    const { name, description, price } = this.props;
    this.setState({ name, description, price });
  }

  handleOpen = (e) => {
    e.stopPropagation();
    this.setState({ modalOpen: true });
  };

  handleClose = (e) => {
    e.stopPropagation();
    this.setState({ modalOpen: false });
  };

  handleFieldChange = (e) => {
    const field = e.target.dataset.title;
    this.setState({ [field]: e.target.value });
  };

  handleSubmit = (e) => {
    e.stopPropagation();
    const {
      state: { name, description, price },
      props: { id, categoryId, updateItem }
    } = this;

    const item = {
      id,
      categoryId,
      name,
      description,
      price
    };

    this.setState({ modalOpen: false }, () => updateItem(item));
  };

  render() {
    const {
      state: {
        modalOpen, name, description, price
      },
      handleOpen,
      handleClose,
      handleFieldChange,
      handleSubmit
    } = this;

    const modalTrigger = (
      <Button color="blue" onClick={handleOpen}>
        <Icon name="add" />
        Edit
      </Button>
    );

    return (
      <Modal
        trigger={modalTrigger}
        open={modalOpen}
        onClose={handleClose}
        onClick={e => e.stopPropagation()}
      >
        <Modal.Header>Edit Item</Modal.Header>

        <Modal.Content>
          <Form>
            <Form.Field>
              <label>Item Name</label>
              <input value={name} data-title="name" onChange={handleFieldChange} />
            </Form.Field>

            <Form.Field>
              <label>Item Description</label>
              <input value={description} data-title="description" onChange={handleFieldChange} />
            </Form.Field>

            <Form.Field>
              <label>Item Price</label>
              <input value={price} data-title="price" onChange={handleFieldChange} />
            </Form.Field>
          </Form>
        </Modal.Content>

        <Modal.Actions>
          <Button color="red" onClick={handleClose}>
            <Icon name="remove" />
            Cancel
          </Button>

          <Button color="green" onClick={handleSubmit}>
            <Icon name="checkmark" />
            Save
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapDispatch = { updateItem };

export default connect(
  null,
  mapDispatch
)(UpdateItem);
