import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Modal, Icon, Form
} from 'semantic-ui-react';

import { createNewItem } from '../../actions';

class CreateItem extends React.Component {
  static propTypes = {
    createNewItem: PropTypes.func.isRequired,

    categoryId: PropTypes.string.isRequired
  };

  state = {
    modalOpen: false,
    name: '',
    description: '',
    price: ''
  };

  handleOpen = (e) => {
    e.stopPropagation();
    this.setState({ modalOpen: true });
  };

  handleClose = (e) => {
    e.stopPropagation();
    this.setState({ modalOpen: false });
  };

  handleFieldChange = (e) => {
    const field = e.target.dataset.title;
    this.setState({ [field]: e.target.value });
  };

  handleSubmit = (e) => {
    e.stopPropagation();
    const {
      state: { name, description, price },
      props: { categoryId, createNewItem }
    } = this;

    const item = {
      id: `${Date.now()}`,
      categoryId,
      name,
      description,
      price
    };

    this.setState(
      {
        modalOpen: false,
        name: '',
        description: '',
        price: ''
      },
      () => createNewItem(item)
    );
  };

  render() {
    const {
      state: {
        modalOpen, name, description, price
      },
      handleOpen,
      handleClose,
      handleFieldChange,
      handleSubmit
    } = this;

    const modalTrigger = (
      <Button color="blue" onClick={handleOpen}>
        <Icon name="add" />
        Add Item
      </Button>
    );

    return (
      <Modal
        trigger={modalTrigger}
        open={modalOpen}
        onClose={handleClose}
        onClick={e => e.stopPropagation()}
      >
        <Modal.Header>Add Item</Modal.Header>

        <Modal.Content>
          <Form>
            <Form.Field>
              <label>Item Name</label>
              <input value={name} data-title="name" onChange={handleFieldChange} />
            </Form.Field>

            <Form.Field>
              <label>Item Description</label>
              <input value={description} data-title="description" onChange={handleFieldChange} />
            </Form.Field>

            <Form.Field>
              <label>Item Price</label>
              <input value={price} data-title="price" onChange={handleFieldChange} />
            </Form.Field>
          </Form>
        </Modal.Content>

        <Modal.Actions>
          <Button color="red" onClick={handleClose}>
            <Icon name="remove" />
            Cancel
          </Button>

          <Button color="green" onClick={handleSubmit}>
            <Icon name="checkmark" />
            Add
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapDispatch = { createNewItem };

export default connect(
  null,
  mapDispatch
)(CreateItem);
