import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './app';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store-configuration';

import 'semantic-ui-css/semantic.min.css';
import './index.css';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  // eslint-disable-next-line no-undef
  document.getElementById('root')
);
registerServiceWorker();
