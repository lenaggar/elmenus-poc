import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import { createLogger } from 'redux-logger';

import rootReducer from './app/reducer';

const logger = createLogger({ collapsed: true });

const middlewares = [thunk, promiseMiddleware(), logger];

const storeEnhancer = compose(applyMiddleware(...middlewares));

const configureStore = () => createStore(rootReducer, storeEnhancer);

export default configureStore;
